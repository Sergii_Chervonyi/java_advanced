package service;

import exception.NotFoundException;
import model.Address;
import model.Animal;
import model.User;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import service.impl.UserServiceImpl;

import java.util.List;

public class UserServiceImpTest {

    private static UserService service;

    @BeforeAll
    static void init() {
        service = new UserServiceImpl();
    }


    @Test
    @DisplayName("Is adult Test")
    public void isAdultTest() throws NotFoundException {
        Assertions.assertTrue(service.isAdult(1));
        Assertions.assertFalse(service.isAdult(2));
        Assertions.assertThrows(NotFoundException.class, () -> service.isAdult(15555));
    }

    @Test
    @DisplayName("Add address test")
    public void addAddressTest() throws NotFoundException {
        int userId = 1;
        Address expected = new Address(userId, "Test Country", "Test city");
        service.addAddress(userId, expected);
        User user = service.getUserById(userId);
        Address actual = user.getAddress();
        Assertions.assertEquals(expected, actual);
    }

    @Test
    @DisplayName("Add animal to user")
    public void addAnimalTest() throws NotFoundException {
        int userId = 1;
        Animal expected = new Animal(5, "Kitty", 1);
        service.addAnimal(userId, expected);
        User user = service.getUserById(userId);
        Assertions.assertTrue(checkIfAnimalIs(user.getAnimals(), expected));
    }

    @Test
    @DisplayName("Delete animal from user")
    public void deleteAnimalTest() throws NotFoundException {
        addAnimalTest();
        int userId = 1;
        Animal expected = new Animal(5, "Kitty", 1);
        service.deleteAnimal(userId, expected.getId());
        Assertions.assertFalse(checkIfAnimalIs(service.getUserById(userId).getAnimals(), expected));
    }


    private boolean checkIfAnimalIs(List<Animal> animals, Animal animalToCheck) {
        boolean is = false;
        for (Animal animal : animals) {
            if (animal.equals(animalToCheck)) {
                is = true;
                return is;
            }
        }
        return is;
    }
}
