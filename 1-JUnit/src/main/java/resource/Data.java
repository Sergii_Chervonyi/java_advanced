package resource;

import model.Address;
import model.Animal;
import model.User;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Data {

    public static List<User> getList(){
        List<User> users = new ArrayList<>();
        users.add(new User(1, "Full Name", 25,  new Address(1, "Ukraine", "Lviv"),
                new ArrayList<>(Arrays.asList(new Animal(1, "Cat", 2), new Animal(2, "Dog", 5)))));
        users.add(new User(2, "Full Name2", 15,  new Address(2, "Ukraine", "Ternopil"),
                new ArrayList<>(Arrays.asList(new Animal(3, "Cat-barsik", 3)))));
        return users;
    }
}
