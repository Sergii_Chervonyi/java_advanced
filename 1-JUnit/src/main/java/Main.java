import exception.NotFoundException;
import model.Address;
import model.Animal;
import service.UserService;
import service.impl.UserServiceImpl;

public class Main {

    public static void main(String[] args) throws NotFoundException {
        UserService service = new UserServiceImpl();

       /* System.out.println(service.isAdult(1));
        System.out.println(service.isAdult(2));*/

        /*service.addAddress(1, new Address(2, "USA", "City"));
        System.out.println(service.getUserById(1));*/

        Animal animal = new Animal(5, "Kitty", 1);
        service.addAnimal(1, animal);

        System.out.println("---BEFORE ----");
        System.out.println(service.getUserById(1));
        service.deleteAnimal(1, animal.getId());
        System.out.println("---AFTER ----");
        System.out.println(service.getUserById(1));


    }
}
