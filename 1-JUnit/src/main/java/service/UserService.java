package service;

import exception.NotFoundException;
import model.Address;
import model.Animal;
import model.User;

public interface UserService {

    boolean isAdult(int userId) throws NotFoundException;

    void addAddress(int userId, Address address) throws NotFoundException;

    void deleteAddress(int userId, int addressId);

    void addAnimal(int userId, Animal animal) throws NotFoundException;

    void deleteAnimal(int userId, int animalId) throws NotFoundException;

    User getUserById(int userId);

    Animal getAnimalById(int animalId, int userId);

}
