package service.impl;

import exception.NotFoundException;
import model.Address;
import model.Animal;
import model.User;
import resource.Data;
import service.UserService;

import java.util.List;
import java.util.Objects;

public class UserServiceImpl implements UserService {

    private List<User> users;

    public UserServiceImpl() {
        users = Data.getList();
    }

    @Override
    public boolean isAdult(int userId) throws NotFoundException {
        User user = getUserById(userId);
        if (Objects.isNull(user)) {
            throw new NotFoundException("User with id " + userId + " not found!");
        } else {
            return user.getAge() >= 18;
        }
    }

    @Override
    public void addAddress(int userId, Address address) throws NotFoundException {
        User user = getUserById(userId);
        if (Objects.isNull(user)) {
            throw new NotFoundException("User with id " + userId + " not found!");
        } else {
            user.setAddress(address);
        }
    }

    @Override
    public void deleteAddress(int userId, int addressId) {

    }

    @Override
    public void addAnimal(int userId, Animal animal) throws NotFoundException {
        User user = getUserById(userId);
        if (Objects.isNull(user)) {
            throw new NotFoundException("User with id " + userId + " not found!");
        } else {
            user.getAnimals().add(animal);
        }
    }

    @Override
    public void deleteAnimal(int userId, int animalId) throws NotFoundException {
        User user = getUserById(userId);
        if (Objects.isNull(user)) {
            throw new NotFoundException("User with id " + userId + " not found!");
        } else {
            Animal animal = getAnimalById(animalId, userId);
            if (Objects.isNull(animal)) {
                throw new NotFoundException("Animal with id " + animalId + " not found!");
            } else {
                user.getAnimals().remove(animal);
            }
        }
    }

    @Override
    public User getUserById(int userId) {
        User user = null;
        for (User u : users) {
            if (u.getId() == userId) {
                user = u;
                break;
            }
        }
        return user;
    }

    @Override
    public Animal getAnimalById(int animalId, int userId) {
        Animal animal = null;
        User user = getUserById(userId);

        for (Animal a : user.getAnimals()) {
            if (a.getId() == animalId) {
                animal = a;
                break;
            }
        }
        return animal;
    }


}
