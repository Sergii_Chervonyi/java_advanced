package model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import util.RandomIdGenerator;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Project {
    private int id;
    private String email;
    private String password;
    private String name;
    private int budget;

    public Project(String email, String password, String name, int budget) {
        this.id = RandomIdGenerator.getRandomID();
        this.email = email;
        this.password = password;
        this.name = name;
        this.budget = budget;
    }

    public Project(String email, String password, String name) {
        this.id = RandomIdGenerator.getRandomID();
        this.email = email;
        this.password = password;
        this.name = name;
    }
}
