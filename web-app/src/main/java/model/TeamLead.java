package model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import util.RandomIdGenerator;

import java.sql.Timestamp;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TeamLead {
    private int id;
    private String fullName;
    private Timestamp dateOfBirth;
    private double salary;

    public TeamLead(String fullName, Timestamp dateOfBirth, double salary) {
        this.id = RandomIdGenerator.getRandomID();
        this.fullName = fullName;
        this.dateOfBirth = dateOfBirth;
        this.salary = salary;
    }

}
