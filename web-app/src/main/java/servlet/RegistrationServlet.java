package servlet;

import lombok.SneakyThrows;
import model.Project;
import org.apache.log4j.Logger;
import service.ProjectService;
import service.impl.ProjectServiceImpl;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.sql.SQLException;
import java.util.Objects;

public class RegistrationServlet extends HttpServlet {
    private ProjectService projectService;
    private static final Logger logger = Logger.getLogger(RegistrationServlet.class);

    public RegistrationServlet() throws SQLException, ClassNotFoundException {
        projectService = new ProjectServiceImpl();
    }

    @SneakyThrows
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) {
        String email = req.getParameter("email");
        if (Objects.isNull(projectService.readByEmail(email))) {
            logger.info("Registration for new Project");
            String projectName = req.getParameter("projectName");
            String password = req.getParameter("password");
            if (!email.isEmpty() && !password.isEmpty() && !projectName.isEmpty()) {
                Project project = new Project(email, password, projectName);
                projectService.create(project);
                logger.info("Project was registered : " + project);
                HttpSession session = req.getSession(true);
                session.setAttribute("projectName", projectName);
                session.setAttribute("projectEmail", email);
            }

        } else {
            logger.info("Project with email : " + email + " already registered! Redirection to email page ... ");
            //TODO : redirect project on email page if email is already in use
        }
        resp.setContentType("text/plain");
        resp.setCharacterEncoding("UTF-8");
        resp.getWriter().write("Success");


    }
}
