package dao.impl;

import dao.ProjectDao;
import model.Project;
import util.MySqlConnector;
import util.SQLConstants;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ProjectDaoImpl implements ProjectDao {
    private Connection connection;

    public ProjectDaoImpl() throws SQLException, ClassNotFoundException {
        connection = MySqlConnector.getConnection();
    }


    @Override
    public List<Project> readAll() throws SQLException {
        List<Project> projects = new ArrayList<>();
        try (Statement statement = connection.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(SQLConstants.GET_ALL_PROJECTS)) {
                while (resultSet.next()) {
                    Project project = new Project(
                            resultSet.getInt("id"),
                            resultSet.getString("email"),
                            resultSet.getString("password"),
                            resultSet.getString("name"),
                            resultSet.getInt("budget"));
                    projects.add(project);
                }
            }
        }

        return projects;
    }

    @Override
    public Project read(int id) throws SQLException {
        ResultSet resultSet = null;
        Project project = null;
        try (PreparedStatement statement = connection.prepareStatement(SQLConstants.GET_PROJECT_BY_ID)) {
            statement.setInt(1, id);
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                project = new Project(resultSet.getInt("id"), resultSet.getString("email"),
                        resultSet.getString("password"),
                        resultSet.getString("name"),
                        resultSet.getInt("budget"));
            }
        } finally {
            resultSet.close();
        }
        return project;
    }

    @Override
    public Project readByEmail(String email) throws SQLException {
        ResultSet resultSet = null;
        Project project = null;
        try (PreparedStatement statement = connection.prepareStatement(SQLConstants.GET_PROJECT_BY_EMAIL)) {
            statement.setString(1, email);
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                project = new Project(resultSet.getInt("id"), resultSet.getString("email"),
                        resultSet.getString("password"), resultSet.getString("name"),
                        resultSet.getInt("budget"));
            }
        } finally {
            resultSet.close();
        }
        return project;
    }

    @Override
    public void create(Project project) throws SQLException {
        try (PreparedStatement statement = connection.prepareStatement(SQLConstants.INSERT_PROJECT)) {
            statement.setInt(1, project.getId());
            statement.setString(2, project.getEmail());
            statement.setString(3, project.getPassword());
            statement.setString(4, project.getName());
            statement.setInt(5, project.getBudget());
            statement.execute();
        }

    }

    @Override
    public void update(int id, Project current) throws SQLException {
        try (PreparedStatement statement = connection.prepareStatement(SQLConstants.UPDATE_PROJECT)) {
            statement.setInt(1, current.getId());
            statement.setString(2, current.getEmail());
            statement.setString(3, current.getPassword());
            statement.setString(4, current.getName());
            statement.setInt(5, current.getBudget());
            statement.setInt(6, id);
            statement.execute();
        }
    }

    @Override
    public void delete(int id) throws SQLException {
        try (PreparedStatement statement = connection.prepareStatement(SQLConstants.DELETE_PROJECT_BY_ID)) {
            statement.setInt(1, id);
            statement.execute();
        }

    }
}
