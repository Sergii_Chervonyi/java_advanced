package dao;

import exception.AlreadyExistException;
import exception.NotFoundException;
import model.Worker;
import shared.AbstractCrudOperations;

import java.sql.SQLException;

public interface WorkerDao extends AbstractCrudOperations<Worker> {
    void update(int id, Worker current) throws SQLException, AlreadyExistException, NotFoundException;
}
