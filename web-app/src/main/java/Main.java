import dao.impl.ProjectDaoImpl;
import dao.impl.TeamLeadDaoImpl;
import dao.impl.WorkerDaoImpl;
import exception.AlreadyExistException;
import exception.NotFoundException;
import model.Project;
import model.TeamLead;
import model.Worker;
import service.impl.ProjectServiceImpl;
import service.impl.TeamLeadServiceImpl;
import service.impl.WorkerServiceImpl;
import shared.AbstractCrudOperations;

import java.sql.SQLException;
import java.sql.Timestamp;

public class Main {
    public static void main(String[] args) throws SQLException, ClassNotFoundException, NotFoundException, AlreadyExistException {
        //ProjectDaoImpl service = new ProjectDaoImpl();
        //service.readAll().forEach(System.out::println);
        //service.create(new Project("Test", 7000));
        //service.update(3, new Project(3,"Test_2", 7000));
        //service.delete(3);

        //TeamLeadDaoImpl teamLeadDao = new TeamLeadDaoImpl();
        //teamLeadDao.readAll().forEach(System.out::println);
        //System.out.println(teamLeadDao.read(1));
        //teamLeadDao.create(new TeamLead(3,"Test", new Timestamp(System.currentTimeMillis()), 1300));
        //teamLeadDao.delete(3);

        //WorkerDaoImpl workerDao = new WorkerDaoImpl();
        //workerDao.readAll().forEach(System.out::println);
        //System.out.println(workerDao.read(2));
        //workerDao.create(new Worker("test", "test", 1000));
        //workerDao.update(5, new Worker("test_2", "test_2", 1000));
        //workerDao.delete(0);

        //ProjectServiceImpl projectService = new ProjectServiceImpl();
        //System.out.println(projectService.read(0));
        //projectService.create(new Project("mainTest@", "1234", "test_name"));
        //projectService.update(4, new Project(5, "TestApp", 500));
        //projectService.delete(50);

        //WorkerServiceImpl workerService = new WorkerServiceImpl();
        //System.out.println(workerService.read(1));
        //workerService.readAll().forEach(System.out::println);
        //workerService.create(new Worker(5, "Test_name", "Test_title", 500, 2));
        //workerService.update(5, new Worker(6, "Test_name2", "Test_title2", 505, 2));
        //workerService.delete(6);

        //TeamLeadServiceImpl teamLeadService = new TeamLeadServiceImpl();
        //teamLeadService.readAll().forEach(System.out::println);
        //System.out.println(teamLeadService.read(1));
        //teamLeadService.create(new TeamLead(3, "Test_Name", new Timestamp(System.currentTimeMillis()),100));
        //teamLeadService.delete(3);









    }
}
