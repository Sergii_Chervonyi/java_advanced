package service;

import dao.ProjectDao;
import model.Project;
import shared.AbstractCrudOperations;

public interface ProjectService extends AbstractCrudOperations<Project>, ProjectDao {

}
