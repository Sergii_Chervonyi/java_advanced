DROP DATABASE web_app;

CREATE DATABASE IF NOT EXISTS web_app;
USE web_app;

CREATE TABLE IF NOT EXISTS project (
	id INT NOT NULL PRIMARY KEY,
    email VARCHAR(45) NOT NULL,
    password VARCHAR(15) NOT NULL,
    name VARCHAR(50) NOT NULL,
    budget double NOT NULL
);

CREATE TABLE IF NOT EXISTS teamLead (
	id INT NOT NULL PRIMARY KEY,
    full_name VARCHAR(50) NOT NULL,
    date_of_birth TIMESTAMP,
    salary double,
    FOREIGN KEY(id) REFERENCES project(id)
);

CREATE TABLE IF NOT EXISTS worker(
	id INT NOT NULL PRIMARY KEY,
    full_name VARCHAR(50) NOT NULL,
    title VARCHAR(50) NOT NULL,
    salary double,
	team_lead_id INT NOT NULL,
	FOREIGN KEY(team_lead_id) REFERENCES teamLead(id)
);

INSERT INTO  project (id, email, password, name, budget) VALUES  (1, '1@i.ua', '111', 'Aaa', 1001), (2, '2@i.ua', '222', 'Aa2', 1002);
INSERT INTO  teamLead (id, full_name, date_of_birth, salary) VALUES  (1, "TT", "1987-01-01", 3001.1), (2, "J Red", "1993-02-02", 3002.2);
INSERT INTO  worker (id, full_name, title, salary, team_lead_id) VALUES  (1, "QQ", "T", 1001.2, 1), (2, "Anna Red", "Ta", 1002, 2);