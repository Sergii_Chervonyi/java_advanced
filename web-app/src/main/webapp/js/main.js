$('.message a').click(function () {
    $('form').animate({
        height: "toggle",
        opacity: "toggle"
    }, "slow");
});


$(document).ready(function () {
    $("button#register").click(function () {
        var projectName = $("form.register-form input.projectName").val();
        var email = $("form.register-form input.email").val();
        var password = $("form.register-form input.password").val();
        var confirmPassword = $("form.register-form input.confirmPassword").val();
        if (projectName == '' || email == '' || password == '' || confirmPassword == '') {
            alert("Please fill all fields...!!!!!!");
        } else if ((password.length) < 8) {
            alert("Password should at least 8 character");
        } else if (!(password).match(confirmPassword)) {
            alert("Your passwords don't match. Try again?");
        } else {
            var projectRegistration = {
                projectName: projectName,
                email: email,
                password: password
            };
            $.post("registration", projectRegistration, function (data) {
                if (data == 'Success') {
                    $("form")[0].reset();
                }
            });
        }
    });
});

$(document).ready(function () {
    $("button#login").click(function () {
        var email = $("form.login-form input.email").val();
        var password = $("form.login-form input.password").val();
        if (email == '' || password == '') {
            alert("Please fill all fields...!!!!!!");
        } else {
            var project = {
                email: email,
                password: password
            };
            $.post("login", project, function (data) {
                if (data != '') {
                    let finalUrl = '';
                    let url = window.location.href.split("/");
                    for (let i = 0; i < url.length - 1; i++) {
                        finalUrl += url[i] + "/";
                    }
                    finalUrl += data.destinationUrl;
                    window.location.href = finalUrl;

                }
                $("form")[1].reset();
            });
        }
    });
});