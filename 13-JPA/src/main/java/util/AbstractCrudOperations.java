package util;

import java.util.List;

public interface AbstractCrudOperations<T> {

    List<T> readAll();

    //static  read(int id) {    }
    //static T read(Serializable id);

    void create(T t);

    void delete(int id);

}
