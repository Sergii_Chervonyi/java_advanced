package model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "reader")
public class Reader {

    @Id
    @Column(name = "reader_id")
    private int ReaderId;
    @Column(name = "full_name")
    private String full_name;
}
