package service;

import model.Reader;
import util.AbstractCrudOperations;

import java.io.Serializable;

public interface ReaderService extends AbstractCrudOperations<Reader> {
    Reader read(Serializable id);
}
