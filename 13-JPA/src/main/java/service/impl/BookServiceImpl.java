package service.impl;

import model.Book;
import service.BookService;
import util.FactoryManager;

import javax.persistence.EntityManager;
import java.io.Serializable;
import java.util.List;

public abstract class BookServiceImpl implements BookService {

    private EntityManager entityManager = FactoryManager.getEntityManager();


    @Override
    public List<Book> readAll() {
        return null;
    }

    @Override
    public Book read(int book_id) {
        return entityManager.find(Book.class, book_id);
    }

    @Override
    public void create(Book book) {

    }

    @Override
    public void delete(int bookid) {

    }

    public Book read(Serializable id) {
        return null;
    }
}
