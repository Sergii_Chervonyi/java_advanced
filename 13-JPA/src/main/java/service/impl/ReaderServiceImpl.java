package service.impl;

import model.Reader;
import service.ReaderService;
import util.FactoryManager;

import javax.persistence.EntityManager;
import java.io.Serializable;
import java.util.List;

public class ReaderServiceImpl implements ReaderService {

    private EntityManager entityManager = FactoryManager.getEntityManager();

    @Override
    public List<Reader> readAll() {
        return null;
    }

    @Override
    public Reader read(Serializable id) {
        return null;
    }

    public Reader read(int book_id) {
        return entityManager.find(Reader.class, book_id);
    }

    @Override
    public void create(Reader reader) {

    }

    @Override
    public void delete(int Readerid) {

    }
}
