package service.impl;

import model.Book;
import model.Reader;
import model.BookReader;
import service.BookReaderService;
import service.ReaderService;
import service.BookService;
import util.AbstractCrudOperations;
import util.FactoryManager;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.io.Serializable;
import java.util.List;

import static service.BookService.*;

public class BookReaderServiceImpl implements BookReaderService {

    private EntityManager entityManager = FactoryManager.getEntityManager();
    private ReaderService readerService = new ReaderServiceImpl();
    private BookService productService = new BookServiceImpl() {
    };
    private int readerId;
    private int bookId;

    @SuppressWarnings("unchecked")
    @Override
    public List<BookReader> readAll() {
        Query query = entityManager.createQuery("SELECT bp FROM BookReader bp");
        return (List<BookReader>) query.getResultList();
    }

    @Override
    public BookReader read(Serializable id) {
        return entityManager.find(BookReader.class, new BookReader());
    }

    @Override
    public BookReader readByIds(int readerId, int bookId) {
        return null;
    }

    @Override
    public void create(BookReader bookReader) {
        
    }

    @Override
    public void delete(int id) {

    }


    /*@Override
    public BookReader readByIds(int bookId, int readerId) {
        //Book book = bookService.read(bookId);
        Book book = (Book) AbstractCrudOperations.read(bookId);
        Reader reader = (Reader) AbstractCrudOperations.read(readerId);
        return entityManager.find(BookReader.class, new BookReader());
    }*/

   /* @Override
    public BookReader readByIds(int bookId, int readerId) {

        return entityManager.find(BookReader.class, new BookReader());
    }*/

    @Override
    public void create(int readerId, int bookId) {
        this.readerId = readerId;
        this.bookId = bookId;
        BookReader reader = read (readerId);
        BookReader book = read(bookId);

        if (!entityManager.getTransaction().isActive()) {
            entityManager.getTransaction().begin();
        }
        entityManager.persist(new BookReader(reader, book));
        entityManager.getTransaction().commit();
    }

    @Override
    public void delete(int readerId, int bookId) {
        BookReader bookReader = readByIds(readerId, bookId);

        if (!entityManager.getTransaction().isActive()) {
            entityManager.getTransaction().begin();
        }
        entityManager.remove(bookReader);
        entityManager.getTransaction().commit();
    }


}
