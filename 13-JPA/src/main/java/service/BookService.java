package service;

import model.Book;
import util.AbstractCrudOperations;

public interface BookService extends AbstractCrudOperations<Book> {
    Book read(int bookId);
}
