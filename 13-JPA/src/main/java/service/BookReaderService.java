package service;

import model.BookReader;
import util.AbstractCrudOperations;

import java.io.Serializable;
import java.util.List;

public interface BookReaderService extends AbstractCrudOperations<BookReader> {

    BookReader read(Serializable id);

    BookReader readByIds(int readerId, int bookId);

    List<BookReader> readAll();

    void create(int readerId, int bookId);

    void delete(int readerId, int bookId);
}
